import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class MyLogger {
  private static MyLogger instance;
  private FileWriter _fw = null;
  private final String _filename = "output.dat";

  private MyLogger() {
    try {
      _fw = new FileWriter(_filename);
    } catch (IOException e) {
        System.out.println("Error accessing file " + _filename + " [" + e + "]. Log will not be recorded");
    }
  }

  public void stop() {
    try {
      if (_fw != null) {
        _fw.close();
      }
    } catch (IOException e) {}
  }

  public static MyLogger getInstance() {
    if (instance == null) {
      instance = new MyLogger();
    }
    return instance;
  }

  public synchronized void log(String s) {
    System.out.println(s);
    try {
      if (_fw != null) {
        _fw.write(s + '\n');
        _fw.flush();
      }
    } catch (IOException e) {

    }
  }
}
