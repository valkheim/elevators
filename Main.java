public class Main {
    private static int _nElevators = 10;
    private static int _nPersons = 50;

    private static void handleArgs(String[] args) {
      if (args.length > 0) {
        _nElevators = Integer.parseInt(args[0]);
      }
      if (args.length > 1) {
        _nPersons = Integer.parseInt(args[1]);
      }
    }

    public static void main(String[] args) {
      handleArgs(args);
      ElevatorControler.getInstance(_nElevators);
      new Person(0).start();
      for (int i = 0; i < _nPersons - 1; i++) {
         new Person().start();
       }
       PersonManager.getInstance().waitForCompletion();
       ElevatorControler.getInstance().shutdown();
       MyLogger.getInstance().stop();
    }
}
