import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

public class PersonManager {
  private static PersonManager _instance;
  private ArrayList<Person> _personList = new ArrayList<Person>();
  private ReentrantLock _lock = new ReentrantLock();
  private Condition _isEmpty = _lock.newCondition();

  private PersonManager() {}

  public void register(Person person) {
    try {
      _lock.lock();
      _personList.add(person);
    } finally {
      _lock.unlock();
    }
  }

  public void delete(Person person) {
    try {
      _lock.lock();
      _personList.remove(person);
      if (_personList.isEmpty()) {
        _isEmpty.signalAll();
      }
    } finally {
      _lock.unlock();
    }
  }

  public void waitForCompletion() {
    try {
      _lock.lock();
      while (!_personList.isEmpty()) {
        _isEmpty.await();
      }
    } catch(InterruptedException e) {
      MyLogger.getInstance().log("Interrupt Exception");
      System.exit(1);
    } finally {
      _lock.unlock();
    }
  }

  public static PersonManager getInstance() {
    if(_instance == null) {
      _instance = new PersonManager();
    }
    return _instance;
  }
}
