import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import java.lang.Math;

public class Elevator extends Thread {
  private ReentrantLock _listLock = new ReentrantLock();
  private Condition _notEmpty = _listLock.newCondition();

  public String _id;

  private final int _maxWeight = 500;
  private int _curWeight = 0;

  public ArrayList<PersonSchedule> _schedule = new ArrayList<PersonSchedule>();

  private int _floor = 0;

  private ArrayList<PersonSchedule> _events = new ArrayList<PersonSchedule>();
  private boolean _isStoringEvent = false;

  private boolean _running = true;


  public Elevator() {
    _id = "E-" + this.getName();
  }

  public void shutdown() {
    _listLock.lock();
    try {
      _running = false;
      _notEmpty.signalAll();
    } finally {
      _listLock.unlock();
    }
  }

  public void fillElevatorScheduleEmulator(ElevatorScheduleEmulator ev){
    _listLock.lock();
    try {
      ev.schedule = new ArrayList<PersonSchedule>(_schedule);
      ev.curWeight = _curWeight;
      ev.floor = _floor;
      ev.maxWeight = _maxWeight;
      _isStoringEvent = true;
    } finally {
      _listLock.unlock();
    }
  }

  public void applyNewSchedule(ElevatorScheduleEmulator updatedValue) {
    _listLock.lock();
    try {
      while (!_events.isEmpty()) {
        updatedValue.schedule.remove(_events.remove(0));
      }
      _isStoringEvent = false;
      _schedule = updatedValue.schedule;
      _notEmpty.signal();
    } finally {
      _listLock.unlock();
    }
  }

  public void abortSchedule() {
    _listLock.lock();
    try {
      _isStoringEvent = false;
      _events.clear();
    } finally {
      _listLock.unlock();
    }
  }

  public void run()
  {
    while (true) {
      _listLock.lock();
      try {
        while (_schedule.isEmpty() && _running) {
          _notEmpty.await();
        }
        if (!_running) {
          return;
        }
        while (!_schedule.isEmpty() && _floor == _schedule.get(0).getFloor()) {
          PersonSchedule removed = _schedule.remove(0);

          if (removed.goingIn) {
            removed.person.getIn(this);
            _curWeight += removed.person.totalWeight();
            if (_curWeight > _maxWeight) {
              MyLogger.getInstance().log("Overweigth Critical " + _curWeight);
              System.exit(1);
            }
          } else {
            removed.person.getOut(this);
            _curWeight -= removed.person.totalWeight();
          }
          if (_isStoringEvent) {
            _events.add(removed);
          }
        }
        if (!_schedule.isEmpty()) {
          _floor += _floor < _schedule.get(0).getFloor() ? 1 : - 1;
          _listLock.unlock();
          sleep(100);
        }
      } catch (InterruptedException e) {
        MyLogger.getInstance().log("Exception " + e);
      } finally {
        if (_listLock.getHoldCount() != 0)
          _listLock.unlock();
      }
    }
  }
}
