import java.util.concurrent.Callable;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class ElevatorScheduleEmulator implements Callable<Void>, Comparable<ElevatorScheduleEmulator>{
  public ArrayList<PersonSchedule> schedule;
  public Elevator _elevator;
  private Person _person;
  public int curWeight;
  public int maxWeight;
  public int floor;
  public int time;


  ElevatorScheduleEmulator(Elevator elevator) {
    _elevator = elevator;
  }

  public void setPerson(Person person) {
    this._person = person;
  }


  public String toString() {
    return "[SCHEDULE] : " + schedule.stream()
        .map( n -> n.toString() )
        .collect( Collectors.joining( ", " ) );
  }

  public void apply() {
    _elevator.applyNewSchedule(this);
  }

  public void abort() {
    _elevator.abortSchedule();
  }

  public int compareTo(ElevatorScheduleEmulator other) {
    return time - other.time;
  }

  private boolean isInBetween(PersonSchedule tryValue, int pos) {
    int startFloor = (pos == 0 ? floor : schedule.get(pos - 1).getFloor());
    int endFloor = schedule.get(pos).getFloor();
    int tryFloor = tryValue.getFloor();

    if (tryValue.goingIn) {
      return ((startFloor <= tryFloor && tryFloor < endFloor) ||
      (endFloor <= tryFloor && tryFloor < startFloor));
    }
    return  ((startFloor <= tryFloor && tryFloor <= endFloor) ||
    (endFloor <= tryFloor && tryFloor <= startFloor));
  }

  public Void call() {
    _elevator.fillElevatorScheduleEmulator(this);

    PersonSchedule[] toInsert = {new PersonSchedule(_person, true), new PersonSchedule(_person, false)};

    for (int i = 0; i < schedule.size() + 1; i++) {
      if (i == schedule.size() || // if end of schedule
      (isInBetween(toInsert[0], i) &&
      (_person.isGoingDown() == ((i == 0 ? floor : schedule.get(i - 1).getFloor()) > schedule.get(i).getFloor())) && // if going the same direction
      (curWeight + _person.totalWeight() <= maxWeight))) { // if Does not overweigth elevator
        int tmpWeight = curWeight + _person.totalWeight();
        for (int j = i; j < schedule.size() + 1; j++) {
          if (j == schedule.size() || isInBetween(toInsert[1], j)) {
            schedule.add(j, toInsert[1]);
            schedule.add(i, toInsert[0]);

            time = Math.abs(floor - schedule.get(0).getFloor());
            for (int k = 0; k < schedule.size() - 2; k++) {
              time += Math.abs(schedule.get(k).getFloor() - schedule.get(k + 1).getFloor());
            }
            return null;
          }
          tmpWeight += schedule.get(j).person.totalWeight() * (schedule.get(j).goingIn ? 1 : -1);
          if (tmpWeight > maxWeight) {
            break;
          }
        }
      }
      curWeight += schedule.get(i).person.totalWeight() * (schedule.get(i).goingIn ? 1 : -1);
    }
    return null;
  }
}
