CLASSES = Main.java \
	Person.java\
	MyLogger.java\
	Elevator.java\
	ElevatorControler.java\
	PersonSchedule.java\
	ElevatorScheduleEmulator.java\
	PersonManager.java

all: classes

classes: $(CLASSES:.java=.class)

.java.class:
	javac $*.java

run:
	java Main 5 5

clean:
	$(RM) *.class
	$(RM) output.dat

.SUFFIXES: .java .class
