
public class PersonSchedule {
  public Person person;
  public boolean goingIn;

  public PersonSchedule(Person person, boolean goingIn) {
    this.person = person;
    this.goingIn = goingIn;
  }

  public int getFloor() {
    return goingIn ? person._arrivalFloor : person._destinationFloor;
  }

  public String toString() {
    return person._id + " " + getFloor() + " "+(goingIn ? "IN" : "OUT");
  }
}
