import java.sql.Timestamp;
import java.util.Random;

public class Person extends Thread {
  public String _id;
  private int _waitingFor;
  public final int _weight = 100;
  public int _arrivalFloor;
  public int _destinationFloor;
  public int _additionalTrolleyWeight = 0;

  public Person(int waitingFor, int arrivalFloor, int destinationFloor, int additionalTrolleyWeight) {
    _id = "P-" + getName();
    _waitingFor = waitingFor;
    _arrivalFloor = arrivalFloor;
    _destinationFloor =  destinationFloor;
    _additionalTrolleyWeight = additionalTrolleyWeight;
    PersonManager.getInstance().register(this);
  }

  public Person(int waitingFor) {
    _id = "P-" + getName();
    _waitingFor = waitingFor;
    _arrivalFloor = generateArrivalFloor();
    _destinationFloor = generateDestinationFloor();
    _additionalTrolleyWeight = generateAdditionalTrolleyWeight();
    PersonManager.getInstance().register(this);
  }

  public Person() {
    _id = "P-" + getName();
    _waitingFor = generateWaitTime();
    _arrivalFloor = generateArrivalFloor();
    _destinationFloor = generateDestinationFloor();
    _additionalTrolleyWeight = generateAdditionalTrolleyWeight();
    PersonManager.getInstance().register(this);
  }

  private int generateWaitTime() {
    return new Random().nextInt(21600);
  }

  private int generateArrivalFloor() {
    return new Random().nextInt(10);
  }

  private int generateDestinationFloor() {
    int dst;
    while ((dst = new Random().nextInt(10)) == _arrivalFloor);
    return dst;
  }

  private int generateAdditionalTrolleyWeight() {
    return new Random().nextInt(5) * 50;
  }

  private void waitForArrival() {
    try {
        sleep(_waitingFor);
    } catch(InterruptedException e) {
      MyLogger.getInstance().log("Interupt Exception");
      System.exit(1);
    }
  }

  public boolean isGoingDown() {
    return _arrivalFloor > _destinationFloor;
  }

  public int totalWeight() {
    return _weight + _additionalTrolleyWeight;
  }

  public String toString() {
    return "Person (" + _id + "), make request at time " + _waitingFor + " starting at floor (" + _arrivalFloor + "), with the destination floor (" + _destinationFloor + "), weight (" + _weight + "+" + _additionalTrolleyWeight + ")";
  }

  public void getOut(Elevator elevator) {
    MyLogger.getInstance().log(_id + " got OUT of " + elevator._id);
    PersonManager.getInstance().delete(this);
  }

  public void getIn(Elevator elevator) {
    MyLogger.getInstance().log(_id + " got IN " + elevator._id);
  }

  public void run()
  {
    waitForArrival();
    System.out.println("Person "+_id+" call Elevator");
    ElevatorControler.getInstance().pushButon(this);
  }
}
