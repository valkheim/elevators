import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ElevatorControler {
  private static ElevatorControler instance;
  private Elevator[] _elevatorList;
  private ArrayList<ElevatorScheduleEmulator> _returnList;
  private final ExecutorService pool;

  private ElevatorControler(Integer nbElevator) {
    pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    _elevatorList = new Elevator[nbElevator];
    _returnList = new ArrayList<ElevatorScheduleEmulator>();

    for (int i = 0; i < nbElevator; ++i) {
      _elevatorList[i] = new Elevator();
      _returnList.add(new ElevatorScheduleEmulator(_elevatorList[i]));
      _elevatorList[i].start();
    }
  }

  public static ElevatorControler getInstance(Integer nbElevator) {
    if(instance == null) {
      instance = new ElevatorControler(nbElevator);
    }
    return instance;
  }

  public static ElevatorControler getInstance() {
    return getInstance(4);
  }

  public void shutdown() {
    pool.shutdownNow();
    try {
      if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
        MyLogger.getInstance().log("Pool could not terminate");
      }
      for (Elevator e : _elevatorList) {
        e.shutdown();
        e.join();
      }
    } catch (InterruptedException e) {

    }
  }

  public synchronized void pushButon(Person person) {
    MyLogger.getInstance().log("Push button " + person.toString());

    for (ElevatorScheduleEmulator el: _returnList) {
      el.setPerson(person);
    }
    try {
      pool.invokeAll(_returnList);
    } catch (InterruptedException e) {
      MyLogger.getInstance().log("Interupt");
      System.exit(1);
    }
    Collections.sort(_returnList);
    MyLogger.getInstance().log(person._id + " Assigned elevator " + _returnList.get(0)._elevator._id);
    _returnList.get(0).apply();
    for (int i = 1; i < _returnList.size(); i++) {
      _returnList.get(i).abort();
    }
  }
}
